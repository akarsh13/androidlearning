package com.bear.findbear;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class recieveMsg extends AppCompatActivity {
    public static final String text="message";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recieve_msg);
        Intent intent=getIntent();
        String messagetext=intent.getStringExtra(text);
        TextView message=(TextView) findViewById(R.id.message);
        message.setText(messagetext);
    }
}
