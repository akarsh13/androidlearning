package com.bear.findbear;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;


public class sendMsg extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_msg);
    }
    public void sendMessage(View view)
    {
        EditText message=(EditText) findViewById(R.id.editText) ;
        String messageText=message.getText().toString();
        Intent intent=new Intent(this,recieveMsg.class);
        intent.putExtra(recieveMsg.text,messageText);
        startActivity(intent);
    }
    public void onmes(View view)
    {
        EditText message=(EditText) findViewById(R.id.editText) ;
        String messageText=message.getText().toString();
        Intent intent=new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,messageText);
        startActivity(intent);
    }
}
