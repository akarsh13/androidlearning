package com.bear.findbear;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class FindBearActivity extends AppCompatActivity {

    ImageView image;
    Button button;
    TextView txt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_bear);
        addOnButton();
    }
    public void onClick(View v)
    {
        txt=(TextView) findViewById(R.id.textView);
        txt.setText("Mil Gya Bhaloo :P");
    }
    public void addOnButton()
    {
        txt=(TextView) findViewById(R.id.textView);
        image = (ImageView) findViewById(R.id.imageView);
        button=(Button) findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                image.setImageResource(R.drawable.naya);
                txt.setText("Pyara Bhaloo");
            }
        });

    }
    public void onSend(View view)
    {
        Intent intent=new Intent(this,sendMsg.class);
        startActivity(intent);
    }

}
